package com.example.nextapp

import android.util.Log
import android.view.textclassifier.TextLanguage
import org.json.JSONObject
import java.io.File
import java.util.*

class Status(val storagePath:File):fileReadWrite{
    val INITIAL= 0
    val NO_USER = 1
    val NO_DEVICE = 2
    val NO_LANGUAGE = 3
    val READY = 4
    private val statusFile="data/statusFile.txt"
    private var lastUser=""
    private var lastDevice=""
    private var lastLanguage=""
    private var appVersion=""
    private var fwVersion=""
    private var savedData=false
    private lateinit var statusJSONObject:JSONObject
    private fun writeStatus(){
        writeLog(packetStatus().toString(),storagePath,statusFile)
        Log.e("WriteStatus",packetStatus().toString())
    }
    fun setInitial(){
        writeStatus()
    }
    fun setUser(user: User){
        lastUser=user.getEmail()
        writeStatus()
    }
    fun setDevice(device:String){
        lastDevice=device
        writeStatus()
    }
    fun setAppVersion(){
        appVersion="1234"
        writeStatus()
    }
    fun setFWVersion(fwversion:String){
        fwVersion=fwversion
        writeStatus()
    }

    fun setLanguage(language: String){
        lastLanguage=language
        writeStatus()
    }
    fun getDevice():String{
        return lastDevice
    }
    private fun readStatus():String{
        val statusFile=read(storagePath,statusFile)
        if (statusFile=="null") return statusFile
        statusJSONObject=JSONObject(statusFile)
        lastUser=statusJSONObject.getString("lastUser")
        lastDevice=statusJSONObject.getString("lastDevice")
        lastLanguage=statusJSONObject.getString("lastLanguage")
        appVersion=statusJSONObject.getString("AppVersion")
        fwVersion=statusJSONObject.getString("FWVersion")
        savedData=statusJSONObject.getBoolean("savedData")
        Log.e("readStatus",statusFile)
        return statusFile
    }
    private fun packetStatus():JSONObject {
        var jsonObject=JSONObject()
        jsonObject.put("timestamp",Date().time)
        jsonObject.put("lastUser",lastUser)
        jsonObject.put("lastDevice",lastDevice)
        jsonObject.put("lastLanguage",lastLanguage)
        jsonObject.put("AppVersion",appVersion)
        jsonObject.put("FWVersion",fwVersion)
        jsonObject.put("savedData",savedData)
        return jsonObject
    }
    fun checkStatus():Int{
        if (readStatus()=="null")return INITIAL
        else if (lastUser.isNullOrEmpty())
            return NO_USER
        else if (lastDevice.isNullOrEmpty())
            return NO_DEVICE
        else if (lastLanguage.isNullOrEmpty())
            return NO_LANGUAGE
        return READY
    }

}