package com.example.nextapp

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.database.DatabaseErrorHandler
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.RequestBody.Companion.toRequestBody
import okio.ByteString.Companion.decodeBase64
import org.json.JSONArray
import org.json.JSONObject
import org.mindrot.jbcrypt.BCrypt
import java.io.File
import java.io.IOException
import java.sql.Date

class User(val storagePath:File,val serverUrl: String):fileReadWrite{
    private var resStr:String=""
    private val personFile="data/personFile.txt"
    ///USER INFORMATION
    private var userid=""
    private var token=""
    private var email=""
    private var password=""
    private var username=""
    private var birthyear=0
    private var height=0
    private var weight=0
    private var drink=0
    private var disease=JSONArray()
    private var license=JSONArray()
    private var carid=""
    ////
    fun Login(input_email:String,input_password:String){
        email=input_email
        password=input_password
        val login_format=JSONObject().put("email",input_email).put("password",input_password)
        val mediaType = "application/json".toMediaType()
        val body = login_format.toString().toRequestBody(mediaType)
        var getdata=serverUrl+"login"
        val client = OkHttpClient()
        val request: Request = Request.Builder()
            .url(getdata)
            .post(body)
            .build()
        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                println("fail : $e")
                Log.e("Error", "connect failed.")
            }
            override fun onResponse(call: Call, response: Response) {
                resStr = response.body?.string().toString()
                Log.e("Res","$resStr")
                val resObject=JSONObject(resStr)
                if(!resObject.isNull("access_token")){
                    token=resObject.getString("access_token")
                    userbyid()
                }
                else{
                    Log.e("resStr",resStr)
                }
            }
        })
    }
    private fun userbyid() {
        val base64_payload=token.split(".")[1]
        var decodeString=base64_payload.decodeBase64()!!.utf8()
        userid=JSONObject(decodeString).getString("sub")
        var getdata = serverUrl+"users/userid=${userid}"
        val client = OkHttpClient()
        val request: Request = Request.Builder()
            .url(getdata)
            .header("Authorization", "Bearer ${token}")
            .get()
            .build()
        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                println("fail : $e")
                Log.e("Error", "connect failed.")
            }

            override fun onResponse(call: Call, response: Response) {
                resStr = response.body?.string().toString()
                Log.e("resStr", resStr)
                val dataArray = JSONObject(resStr).getString("data")
                Log.e("dataArray",dataArray)
                val userData=JSONObject(JSONArray(dataArray)[0].toString())
                username=userData.getString("_id")
                birthyear=userData.getInt("birthyear")
                height=userData.getInt("height")
                weight=userData.getInt("weight")
                drink=userData.getInt("drink")
                disease=userData.getJSONArray("disease")
                license=userData.getJSONArray("license")
                getCarid()
            }
        })
    }
    private fun getCarid(){
        val client = OkHttpClient()
        var carByUserid=serverUrl+"/userid="+userid
        val request: Request = Request.Builder()
            .url(carByUserid)
            .header("Authorization","Bearer ${token}")
            .get()
            .build()
        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                println("fail : $e")
                Log.e("Error", "connect failed.")
            }
            override fun onResponse(call: Call, response: Response) {
                val CarResStr = response.body?.string().toString()
                if(!JSONObject(CarResStr).isNull("data")){
                    var car=JSONArray(JSONObject(CarResStr).getString("data"))[0].toString()
                    carid=JSONObject(car).getString("_id")
                    pack_personalfile()
                }
                else
                    car_fetchJSON()
            }
        })
    }
    private fun car_fetchJSON(){
        var fetchObject= JSONObject()
        fetchObject.put("plate", "DEFAULT")/////string
        fetchObject.put("vehicle", 0)
        fetchObject.put("energy", 0)
        fetchObject.put("brand", "")
        fetchObject.put("model", "")
        fetchObject.put("tonnage",0)
        fetchObject.put("userid",userid)
        fetchObject.put("timestamp", java.util.Date().time)
        val client = OkHttpClient()
        val mediaType = "application/json".toMediaType()
        val body = fetchObject.toString().toRequestBody(mediaType)
        val request: Request = Request.Builder()
            .url(serverUrl+"carinfos")
            .header("Authorization","Bearer ${token}")
            .post(body)
            .build()
        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                println("fail : $e")
                Log.e("Error", "$e")
            }

            override fun onResponse(call: Call, response: Response) {
                val CarResStr = response.body?.string().toString()
                Log.e(" CarInfo", "Succeed  ${CarResStr}")
                carid=CarResStr.substring(1,25)
                pack_personalfile()
            }
        })
    }
    private fun pack_personalfile(){
        var file = File(storagePath, personFile)
        var filestring = file.readText(Charsets.UTF_8)
        var personObject=JSONObject()
        personObject.put("userid",userid)
        personObject.put("carid",carid)
        personObject.put("email",email)
        personObject.put("password",password)
        personObject.put("username",username)
        personObject.put("height",height)
        personObject.put("weight",weight)
        personObject.put("birthyear",birthyear)
        personObject.put("drink",drink)
        personObject.put("disease",disease)
        personObject.put("license",license)
        Log.e("CHECK",personObject.toString())
        try {
            if (filestring.equals("null")) {
                writeLog("[$personObject]",storagePath,personFile)
            }
            else{
                filestring=filestring.removePrefix("[").removeSuffix("]")
                filestring=filestring+","+personObject
                writeLog("[$filestring]",storagePath,personFile)
            }
        } catch (e:IOException) {
            Log.e("Error","$e")
            e.printStackTrace();
        }
    }
    fun getEmail():String{
        return email
    }
    fun getToken():String{
        return token
    }
}
